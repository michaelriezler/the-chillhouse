
/*
 * Collection
*/
export let COLLECTION_INIT = 'COLLECTION_INIT'

/*
 * User
*/
export let USER_INIT = 'USER_INIT'

/*
 * Items
*/
export let ITEMS_ADD = 'ITEMS_ADD'
export let ITEM_SELECTED = 'ITEM_SELECTED'


/*
 * Cart
*/

export let CART_ADD = 'CART_ADD'
export let CART_REMOVE  = 'CART_REMOVE'
export let CART_PAY_CASH  = 'CART_PAY_CASH'


/*
 * Menu
*/

export let MENU_INIT = 'MENU_INIT'


/*
 * Service
*/
export let SERVICE_INIT = 'SERVICE_INIT'
export let SERVICE_SET_ITEMS = 'SERVICE_SET_ITEMS'

/*
 * Home
*/
export let HOME_SET_SPECIAL = 'HOME_SET_SPECIAL'
export let HOME_SET_POPULAR = 'HOME_SET_POPULAR'


/*
 * Bottom Drawer
*/
export let BOTTOM_DRAWER_CLOSE = 'BOTTOM_DRAWER_CLOSE'
export let BOTTOM_DRAWER_OPEN = 'BOTTOM_DRAWER_OPEN'


/*
 * Bottom Drawer
*/
export let MODAL_CLOSE = 'MODAL_CLOSE'
export let MODAL_OPEN = 'MODAL_OPEN'