import React from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { IconButton } from '@rmwc/icon-button'
import { Button } from '@rmwc/button'
import { Typography } from '@rmwc/typography'
import { Elevation } from '@rmwc/elevation'
import Header from 'Component/Header'
import ArrowBack from 'Component/GoBack'
import { CartAction } from 'ActionCreator'

let NAV_HEIGHT = 80
let BottomNav = styled(Elevation).attrs({ z: 3 })`
  position: fixed;
  height: ${NAV_HEIGHT}px;
  bottom: 0;
  left: 0;
  width: 100vw;
  background: var(--mdc-theme-secondary);
`

let NavSection = styled.div`
  display: flex;
  width: 100vw;
  height: 40px;
  padding: 0 24px;
  align-items: center;
  justify-content: flex-end;
`

let OrderBtn = styled(Button)`
  width: 100vw;
  height: 40px!important;
  border-radius: 0!important;
`

let Total = styled(Typography)`
  margin-right: 16px;
  color: var(--mdc-theme-primary-light);
`

let ItemsList = styled.ol`
  padding-left: 24px;
  padding-bottom: 16px;
  margin-bottom: 0;
`

let ListItem = styled.li`
  list-style-type: none;
  margin-bottom: 16px;
  display: flex;
  align-items: center;

  :last-child {
    margin-bottom: 0;
  }
`

let Index = styled(Typography)`
  margin-right: 16px;
  width: 24px;
  flex-shrink: 0;
`

let Amount = styled(Typography)`
  magin-left: auto;
  width: 24px;
  flex-shrink: 0;
`

let Name = styled(Typography)`
  width: 100%;
  padding-right: 16px;
`

let Cart = ({ items, history }) => {
  let total = items.reduce((acc, x) => acc + x.item.price * x.amount, 0)
  let onOrder = () => history.push('/order/confirm')
  let onBack = () => history.goBack()

  return (
    <React.Fragment>
      <div style={{ height: 56, flexShrink: '0' }}></div>

      <ItemsList>
        { items.map((x, i) => 
          <ListItem>
            <Index  use='body2'>{i + 1}.</Index>
            <Name   use='body2'>{x.item.name}</Name>
            <Amount use='body2'>{x.amount}</Amount>
            <IconButton icon='delete' onClick={() => CartAction.remove(x.item.id)}/>
          </ListItem>
        )}
      </ItemsList>

      <div style={{ height: NAV_HEIGHT, flexShrink: '0' }} ></div>

      <BottomNav>
        <NavSection>
          <Total use='body1'>Total:</Total>
          <Typography use='body1'>{ 
            Number(total).toLocaleString('en', {
              style: 'currency',
              currency: 'USD'
            })}</Typography>
        </NavSection>
        <OrderBtn raised onClick={onOrder}>Order</OrderBtn>
      </BottomNav>
      <Header
        left={<ArrowBack onClick={onBack} />}
      ></Header>
    </React.Fragment>
  )
}


export default connect(state => {
  return {
    items: state.Cart.items || []
  }
})(Cart)