import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import BodyCopy from 'Component/BodyCopy'
import { Elevation } from '@rmwc/elevation'
import { Typography } from '@rmwc/typography'
import { Button } from '@rmwc/button'
import { CartAction } from 'ActionCreator'
import useScrollTop from 'Hooks/useScrollTop'
import { getItemBySlug } from 'Data/Http/Item'
import { ItemAction } from 'ActionCreator'
import Header from 'Component/Header'
import ShoppingCart from 'Component/ShoppingCart'
import ArrowBack from 'Component/GoBack'

let BottomBar = styled(Elevation).attrs({ z: 3 })`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100vw;
  height: 40px;
  background: var(--mdc-theme-secondary);
  display: flex;
`

let StyledButton = styled(Button)`
  width: 68.2%;
  background: var(--mdc-theme-primary)!important;
  color: var(--mdc-theme-secondary)!important;
  height: 40px!important;
  border-radius: 0!important;
`

let Input = styled.input.attrs({ type: 'number' })`
  width: 31.8%;
  text-align: center;
  border: 1px solid var(--mdc-theme-primary);
  background: var(--mdc-theme-background);
`

let TitleWrapper = styled.header`
  padding: 16px;
  padding-top: 80px;
  text-align: center;

  h1  {
    margin-top: 0;
  }
`

let Description = styled(BodyCopy)`
  padding-top: 0;
`

let Item = ({ history, match, item }) => {
  let [amount, setAmount] = useState(1)
  let onChange = ({ target: t }) => setAmount(t.value)
  useScrollTop()
  let onBack = () => history.goBack()

  let { collection_slug, slug } = match.params
  useEffect(() => {
    getItemBySlug(collection_slug, slug).then(r => {
      ItemAction.select(r.data)
    })
  }, [collection_slug, slug])

  return (
    <React.Fragment>

      <TitleWrapper>
        <Typography use='headline5' tag='h1'>{item.name}</Typography>
      </TitleWrapper>

      <Description>
        {item.description}
        
        <h4>Ingredients:</h4>
        <ul>
          { (item.ingredients || []).map(i => 
              <li>{i}</li>
          )}
        </ul>
      </Description>

      <div
        style={{ height: 40 + 36, flexShrink: '0' }}
      />

      <BottomBar>
        <Input onChange={onChange} value={amount} />
        <StyledButton
          onClick={_ => {
            CartAction.add(item, amount)
            history.goBack()
          }}
        >Add To Cart</StyledButton>
      </BottomBar>
      <Header
        left={<ArrowBack onClick={onBack} />}
        right={<ShoppingCart />}
      ></Header>
    </React.Fragment>
  )
}

export default connect(state => {
  return {
    item: state.Items.selected || {}
  }
})(Item)