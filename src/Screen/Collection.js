import React, { useEffect } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { ItemAction, CollectionAction } from 'ActionCreator'
import { connect } from 'react-redux'
import * as C from 'Data/Http/Collection'
import Nav from 'Component/Nav'
import NavigationIcon from 'Component/NavigationIcon'
import { Typography } from '@rmwc/typography'
import ShoppingCart from 'Component/ShoppingCart'

let Collection = ({
  history,
  collection,
  match,
  items
}) => {
  useEffect(() => {
    if (collection.id === undefined) {
      C.get_by_slug(match.params.slug)
        .then(r => {
          CollectionAction.init([r.data])
        })
    }
  }, [collection.id === undefined])

  useEffect(() => {
    if (collection.id !== undefined && items.length === 0) {
      C.get_items(collection.id).then(r => {
        ItemAction.add(collection.id, r.data)
      })
    }
  }, [collection.id, items])

  return (
    <React.Fragment>
      <Nav
        slot={{
          right: () => <NavigationIcon
            onClick={() => history.goBack()}
            icon='arrow_back'
          />,
          left: () => <ShoppingCart />
        }}
        title={collection.name}
      />

      <div style={{ paddingTop: 56 }}>
        <List twoLine>
          { items.map(item =>
            <StyledLink
              to={`/item/${collection.slug}/${item.slug}`}
              key={item.id}
            >
              <ListItem>
                <NameWrapper>
                  <ItemName>
                  { item.name}
                  </ItemName>

                  <Price>{ item.price }</Price>
                </NameWrapper>
                <ItemDescription>
                  { item.short_description }
                </ItemDescription>
              </ListItem>
            </StyledLink>
          )}

        </List>
      </div>
    </React.Fragment>
  )
}

export default connect((state, { match }) => {
  let collection = state.Collection.find(c => c.slug === match.params.slug) || {}
  let items = collection.id !== undefined
    ? state.Items.collection[collection.id]
    : []

  return {
    collection,
    items: items || []
  }
})(Collection)

let List = styled.ul`
  padding-left: 24px;
  padding-right: 70px;
`

let ListItem = styled.li`
  color: var(--mdc-theme-on-secondary);
  margin-bottom: 24px;
  list-style-type: none;
`

let StyledLink = styled(Link)`
  text-decoration: none;
`

let ItemName = styled(Typography).attrs({ use: 'body1', tag: 'p' })`
  margin: 0;
  flex-shrink: 0;
  width: 100%;
`

let ItemDescription = styled(Typography).attrs({ use: 'caption' })`
  color: var(--mdc-theme-primary-light);
`

let NameWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`

let PriceText = styled(Typography)`
  position: relative;
  right: -8px;
  bottom: 0;
`

let Price = ({ children }) => {
  let price = children.toLocaleString('en', {
    style: 'currency',
    currency: 'USD'
  })
  return <PriceText>{price}</PriceText>
}