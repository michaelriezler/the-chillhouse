import React, { useEffect } from 'react'
import styled from 'styled-components'
import getMenu from 'Data/Http/Menu'
import List, { Item as ListItem } from 'Component/List'
import Nav from 'Component/Nav'
import { Typography } from '@rmwc/typography'
import { Link } from 'react-router-dom'
import { Button } from '@rmwc/button'
import Header from 'Component/Header'
import { MenuAction, CartAction } from 'ActionCreator'
import { connect } from 'react-redux'
import ShoppingCart from 'Component/ShoppingCart'

let StyledFilter = styled.div`
  position: fixed;
  top: 48px;
  width: 100vw;
  left: 0;
  height: 40px;
  display: flex;
  overflow-y: scroll;
  flex-wrap: nowrap;
  background: #fff;
  align-items: center;
  z-index: 1;
  padding: 0 12px;
`

let Filters = ({ children }) => {
  let handleClick = handle => e => {
    e.preventDefault()
    let node = document.querySelector(`#js-scrollto-${handle}`)
    window.scroll({
      top: node.offsetTop - 88 - 16,
      behavior: "smooth"
    })
  }

  let c = React.Children.map(children, node => {
    return React.cloneElement(node, {
      onClick: handleClick(node.props.query)
    })
  })

  return (
    <StyledFilter>
      { c }
    </StyledFilter>
  )
}

let Filter = styled(Button)`
 margin: 0 4px;
 flex-shrink: 0;
`

let Section = styled.section`
  margin-bottom: 56px;
`

let AddToCart = styled(Button)`
  height: 24px!important;
  min-width: auto!important;
  margin-left: auto;
`

let Wrapper = styled.div`
  --mdc-theme-primary: var(--highlight);  x
  position: relative;
  display: flex;
  flex-direction: column;
  padding-right: 16px;
  width: 70px;
  flex-shrink: 0;
  margin-left: 8px;
`


let Menu =({ menu }) => {

  useEffect(() => {
    getMenu().then(res => {
      MenuAction.init(res.data)
    })
  }, [false])

  return (
    <React.Fragment>
      <Filters>
        {
          menu.map(c => <Filter key={c.slug} query={c.slug}>{c.name}</Filter>)
        }
      </Filters>

      <MenuWrapper>
        { 
          menu.map(collection => {
            let { name, items, slug } = collection
            return (
              <Section id={`js-scrollto-${slug}`} key={slug}>
                <SectionHeader>
                  <Typography use='headline5' tag='h3'>{name}</Typography>
                </SectionHeader>
                <div>
                  <StyledList twoLine>
                    { items.map(item => {

                      let onClick = (e) => {
                        e.preventDefault()
                        CartAction.add(item, 1)
                      }

                      return (
                        <StyledLink
                          to={`/item/${collection.slug}/${item.slug}`}
                          key={item.id}
                        >
                          <StyledListItem>
                            <div>
                              <ItemName>{ item.name}</ItemName>
                              <ItemDescription>{ item.short_description }</ItemDescription>
                            </div>

                            <Wrapper onClick={onClick}>
                              <Price>{ item.price }</Price>
                              <AddToCart outlined>+</AddToCart>
                            </Wrapper>
                          </StyledListItem>
                        </StyledLink>
                      )
                    })}

                  </StyledList>
                </div>
              </Section>
            )
          })
        }
      </MenuWrapper>

      <Nav></Nav>
      <Header
        right={<ShoppingCart />}
      ></Header>
    </React.Fragment>
  )
}

export default connect(state => {
  return {
    menu: state.Menu || []
  }
})(Menu)

let MenuWrapper = styled.main`
  margin-bottom: 24px;
  margin-top: 80px;
`

let StyledLink = styled(Link)`
  text-decoration: none;
`

let ItemName = styled(Typography).attrs({ use: 'body1', tag: 'p' })`
  margin: 0;
  flex-shrink: 0;
  width: 100%;
`

let ItemDescription = styled(Typography).attrs({ use: 'caption' })`
  color: var(--mdc-theme-primary-light);
`

let StyledList = styled(List)`
  padding-right: 0;
`

let StyledListItem = styled(ListItem)`
  display: flex;
  justify-content: space-between;
`

let PriceText = styled(Typography)`
  width: 100%;
  text-align: right;
  margin-bottom: 4px;
`

let SectionHeader = styled.header`
  padding-left: 24px;
  padding-right: 70px;
`

let Price = ({ children }) => {
  let price = children.toLocaleString('en', {
    style: 'currency',
    currency: 'USD'
  })
  return <PriceText>{price}</PriceText>
}