import React, { useContext, useEffect } from 'react'
import { connect } from 'react-redux'
import BodyCopy from 'Component/BodyCopy'
import styled from 'styled-components'
import { Button } from '@rmwc/button'
import Nav from 'Component/Nav'
import Header from 'Component/Header'
import useScrollTop from 'Hooks/useScrollTop'
import AppConfig from 'Context/AppConfig'
import { Typography } from '@rmwc/typography'
import * as S from 'Data/Http/Services'
import * as H from 'Data/Http/Home'
import { ServiceAction, HomeAction } from 'ActionCreator'
import ServiceIcon from 'Component/Icon/Services'
import { Link } from 'react-router-dom'
import ServiceCard from 'Component/ServiceCard'

let RequestPayBtn = styled(Button)`
  margin-top: 16px;
  width: 100%;
  margin-bottom: 32px;
`

let ButtonWrapper = styled.div`
  padding: 0 24px;
`

let Greeting = styled(Typography)`
  margin-bottom: 16px;
  margin-left: 24px;
`

let ServiceWrapper = styled.section`
  display: flex;
  flex-wrap: wrap;
  padding: 0 24px;
  justify-content: space-between;
  margin-bottom: 24px;
`

let StyledService = styled(Link)`
  width: 33.33%;
  height: 0;
  padding-bottom: 30%;
  flex-shrink: 0;
  margin-bottom: 16px;
  position: relative;
  color: inherit;
`

let ServiceContent = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 8px;
  justify-content: space-around;
  text-transform: uppercase;
`

let ServiceTitle = styled(Typography).attrs({ use: 'caption '})`
  width: 100%;
  text-align: center;
  display: inline-block;
`

let Icon = styled.div`
  svg {
    width: 40px;
    height: 40px;
    fill: var(--highlight);
  }
`

let Service = ({ title, icon, slug }) => {
  return (
    <StyledService to={`/services/${slug}`}>
      <ServiceContent>
        <Icon>
          <ServiceIcon icon={slug} />
        </Icon>
        <ServiceTitle>{ title }</ServiceTitle>
      </ServiceContent>
    </StyledService>
  )
}

let Section = styled.section`
  padding: 0 24px;
  margin-bottom: 32px;
`

let HorizontalList = styled.ol`
  display: flex;
  overflow-y: scroll;
  scroll-snap-type: x mandatory;
  padding: 0;
  width: 100vw;
  margin-left: -24px;
`

let ListItem = styled.li`
  width: 80vw;
  flex-shrink: 0;
  scroll-snap-align: center;
  margin-right: 24px;
  list-style-type: none;
`

let SectionTitle = styled(Typography).attrs({ use: 'headline5' })`
  text-transform: uppercase!important;
`


let Home = (p) => {
  let config = useContext(AppConfig)
  useScrollTop()

  useEffect(() => {
    S.get_list(config.id).then(res => ServiceAction.init(res.data))
    H.get_special_offers().then(res => HomeAction.set_special(res.data))
    H.get_popular().then(res => HomeAction.set_popular(res.data))
  }, [config.id])

  return (
    <React.Fragment>

      <div
        style={{ height: 64, flexShrink: '0' }}
      />

      {
        /*{ p.order.id &&
          <ButtonWrapper>
            <RequestPayBtn raised>Request to pay</RequestPayBtn>
          </ButtonWrapper>
        }*/
      }

      <Greeting use="headline4">Hi Alexander,</Greeting>
      <BodyCopy>{config.description}</BodyCopy>

      <ServiceWrapper>
        { p.services.map(s => 
            <Service
              key={s.id}
              { ...s }
            ></Service>
          )}
      </ServiceWrapper>


      <Section>
        <SectionTitle>Special Offers</SectionTitle>
        <HorizontalList>
          <div
            style={{
              width: 24,
              flexShrink: 0,
            }}
          ></div>

          { p.special_offers.map(offer => 
              <ListItem key={offer.id}>
                <ServiceCard
                  img={''}
                  title={offer.name}
                  action={<Button outlined>Book now</Button>}
                  slug={offer.slug}
                ></ServiceCard>
              </ListItem>
          ) }

          <div
            style={{
              width: 24,
              flexShrink: 0,
            }}
          ></div>
        </HorizontalList>
      </Section>


      <Section>
        <SectionTitle>Popular</SectionTitle>
        <HorizontalList>
          <div
            style={{
              width: 24,
              flexShrink: 0,
            }}
          ></div>

          { p.popular.map(offer => 
              <ListItem key={offer.id}>
                <ServiceCard
                  img={''}
                  title={offer.name}
                  action={<Button outlined>Book now</Button>}
                  slug={offer.slug}
                ></ServiceCard>
              </ListItem>
          ) }

          <div
            style={{
              width: 24,
              flexShrink: 0,
            }}
          ></div>
        </HorizontalList>
      </Section>

      <div
        style={{ height: 56, flexShrink: 0 }}
      ></div>

      <Nav />
      <Header />
    </React.Fragment>
  )
}

export default connect(state => {
  return {
    collection: state.Collection,
    user: state.User,
    location: 'dummy-location-id',
    order: state.Cart.order,
    services: state.Service.services,
    special_offers: state.Home.special_offers,
    popular: state.Home.popular,
  }
})(Home)