import React from 'react'
import styled from 'styled-components'
import { Typography } from '@rmwc/typography'
import { Button } from '@rmwc/button'
import { CartAction } from 'ActionCreator'

let Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: 32px;
`

let ButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

let PayLater = styled(Button)`
  margin-bottom: 16px;
  color: #000;
`

let Message = styled(Typography)`
  margin-bottom: 56px;
  text-align: center;
`

export default ({ history }) => {
  let onPayCash = () => {
    CartAction.payCash()
    history.push('/home')
  }

  return (
    <Wrapper>
      <Message use='headline5'>
        We've received your order #123 for table #1
      </Message>

      <ButtonWrapper>
        <PayLater
          raised
          onClick={onPayCash}
        >Pay By Cash</PayLater>
        <Button>Pay Online</Button>
      </ButtonWrapper>
    </Wrapper>
  )
}