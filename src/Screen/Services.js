import React, { useEffect} from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { get_items } from 'Data/Http/Services'
import { ServiceAction } from 'ActionCreator'
import Nav from 'Component/Nav'
import Header from 'Component/Header'
import ServiceCard from 'Component/ServiceCard'
import { Button } from '@rmwc/button'
  
let ContentWrapper = styled.main`
  padding: 0 24px;
`

let Li = styled.li`
  list-style-type: none;
  margin-bottom: 40px;
`

let List = styled.ol`
  padding: 0;
`

let Services = ({ match, items }) => {

  useEffect(() => {
    let { slug } = match.params
    get_items(match.params.slug)
      .then(r => ServiceAction.set_items(slug, r.data))
  }, [match.params.slug])

  return (
    <React.Fragment>
      <div
        style={{ height: 64, flexShrink: '0' }}
      />

      <ContentWrapper>
        <List>
          { items.map(item => {
            return (
              <Li key={item.id}>
                <ServiceCard
                  img={''}
                  title={item.name}
                  action={<Button outlined>Book now</Button>}
                  slug={item.slug}
                ></ServiceCard>
              </Li>
            )
          }) }
        </List>

      </ContentWrapper>

      <div
        style={{ height: 56, flexShrink: 0 }}
      ></div>

      <Nav />
      <Header title={match.params.slug} />
    </React.Fragment>
  )
}

export default connect((state, { match }) => {
  return {
    items: state.Service.items[match.params.slug] || []
  }
})(Services)