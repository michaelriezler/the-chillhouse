import React, { useEffect, useState, useContext } from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import * as U from 'Data/Http/User'
import { UserAction } from 'ActionCreator'
import App from 'App'
import { withRouter } from 'react-router-dom'
import { LinearProgress } from '@rmwc/linear-progress'
import { Typography } from '@rmwc/typography'
import AppConfig from 'Context/AppConfig'

let LoadingWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 32px;
  background: var(--loading-screen-background, #fafafa);
`

let Logo = styled.img`
  max-width: 80%;
  margin-top: -100px;
`

let Name = styled(Typography).attrs({ tag: 'p', use: 'body1'})`
  position: absolute;
  bottom: 32px;
  width: 100vw;
  text-align: center;
`

let StyledProgress = styled(LinearProgress)`
  --mdc-theme-primary: var(--highlight);
`

let Loading = ({ logo, name, onClick }) => {

  return (
    <React.Fragment>
      <StyledProgress determinate={false} />
      <LoadingWrapper>
        <Logo src={logo} />
        <Name>{name}</Name>
      </LoadingWrapper>
    </React.Fragment>
  )
}

let Bootstarp = p => {
  let [loading, setLoading] = useState(true)
  let config = useContext(AppConfig)

  useEffect(() => {
    U.getInfo(p.id).then(r => {
      UserAction.init(r.data)
    })
  }, [p.user_id])

  useEffect(() => {
    setTimeout(() => setLoading(false), 1500)
  }, [])

  if (p.user.id === null || loading) {
    return <Loading {...config} />
  }

  return <App />
}

export default withRouter(connect((state) => {
  return {
    user: state.User
  }
})(Bootstarp))
