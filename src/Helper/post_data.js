let baseURL = process.env.NODE_ENV === 'development'
  ? 'http://localhost:4000'
  : 'https://us-central1-mochi-komune.cloudfunctions.net/app'

export default function postData(url = ``, data = {}) {
    return fetch(`${baseURL}${url}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
    })
    .then(response => response.json())
    .then(data => ({ data }))
}