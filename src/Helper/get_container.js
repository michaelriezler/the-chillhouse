
export default (id) => {
  let container = document.querySelector(`#${id}`)

  if (container === null) {
    let container = document.createElement('div')
    container.id = id
    document.body.appendChild(container)
    return container
  }

  return container
}