import { useEffect, useState } from 'react'

export default () => {
  let [mounted, setMounted] = useState(false)
  useEffect(() => {
    if (mounted) return
    window.scrollTo(0, 0)
    setMounted(true)
  })
}