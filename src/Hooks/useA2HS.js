import { useState, useEffect } from 'react'
import { BottomDrawerAction, ModalAction } from 'ActionCreator'

export default function useA2HS () {
  let [prompt, setPrompt] = useState({
    prompt () {
      ModalAction.open()
      BottomDrawerAction.close()
    }
  })

  useEffect(() => {
    window.addEventListener('beforeinstallprompt', (e) => {
      console.log('beforeinstallprompt', e)
      e.preventDefault()
      setPrompt(e)
    })
  }, [])

  return prompt
}
