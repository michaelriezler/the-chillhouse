import { HOME_SET_SPECIAL, HOME_SET_POPULAR } from 'Action'

let default_state = {
  special_offers: [],
  popular: []
}

export default (state = default_state, action) => {
  switch (action.type) {
    case HOME_SET_SPECIAL:
      return {
        ...state,
        special_offers: action.special_offers
      }

    case HOME_SET_POPULAR:
      return {
        ...state,
        popular: action.popular        
      }

    default:
      return state
  }
}