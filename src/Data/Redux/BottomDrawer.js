import { BOTTOM_DRAWER_OPEN, BOTTOM_DRAWER_CLOSE } from 'Action'

export default (state = false, action) => {
  switch (action.type) {
    case BOTTOM_DRAWER_OPEN:
      return true

    case BOTTOM_DRAWER_CLOSE:
      return false

    default:
      return state
  }
}