import { ITEMS_ADD, ITEM_SELECTED } from 'Action'

let defaultState = {
  collection: {},
  selected: {}
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case ITEMS_ADD:
      return {
        ...state,
        collection: {
          ...state.collection,
          [action.collection]: action.items
        }
      }

    case ITEM_SELECTED:
      return {
        ...state,
        selected: action.selected
      }

    default:
      return state
  }
}
