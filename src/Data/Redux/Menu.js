// @flow
import { MENU_INIT } from 'Action'

let defaultState = []

export default function MenuReducer (state = defaultState, action) {
  switch (action.type) {
    case MENU_INIT:
      return action.menu

    default:
      return state
  }
}