// @flow
import { COLLECTION_INIT } from 'Action'

export type Collcetion = {
  id: number,
  name: string,
  logo: string,
  description: string,
  slug: string
};

let defaultState = []

export default function CollectionReducer (state: Array<Collcetion> = defaultState, action) {
  switch (action.type) {
    case COLLECTION_INIT:
      return action.collections

    default:
      return state
  }
}