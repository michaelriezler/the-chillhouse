import { USER_INIT } from 'Action'

let defaultState = {
  id: null,
  name: '',
  logo: '',
  description: ''
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case USER_INIT:
      return action.user

    default:
      return state
  }
}
