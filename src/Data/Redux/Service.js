import { SERVICE_INIT, SERVICE_SET_ITEMS } from 'Action'

let default_state = {
  services: [],
  items: {}
}

export default (state = default_state, action) => {
  switch (action.type) {
    case SERVICE_INIT:
      return {
        ...state,
        services: action.services
      }

    case SERVICE_SET_ITEMS: {
      return {
        ...state,
        items: {
          ...state.items,
          [action.slug]: action.items
        }
      }
    }

    default:
      return state
  }
}