import { CART_ADD, CART_REMOVE, CART_PAY_CASH } from 'Action'

let default_state = {
  items: [],
  order: {
    id: null,
    payed: false
  }
}

export default (state = default_state, action) => {
  switch (action.type) {
    case CART_ADD:
      return {
        ...state,
        items: [...state.items, action.payload]
      }

    case CART_REMOVE:
      return {
        ...state,
        items: state.items.filter(i => i.item.id !== action.id)
      }

    case CART_PAY_CASH:
      return {
        items: [],
        order: {
          id: '123',
          payed: false
        }
      }

    default:
      return state
  }
}
