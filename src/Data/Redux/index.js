import { createStore, combineReducers } from 'redux'
import Collection from 'Data/Redux/Collection'
import User from 'Data/Redux/User'
import Items from 'Data/Redux/Items'
import Cart from 'Data/Redux/Cart'
import Menu from 'Data/Redux/Menu'
import Service from 'Data/Redux/Service'
import Home from 'Data/Redux/Home'
import BottomDrawer from 'Data/Redux/BottomDrawer'
import Modal from 'Data/Redux/Modal'

let reducer = combineReducers({
  Collection,
  User,
  Items,
  Cart,
  Menu,
  Service,
  Home,
  BottomDrawer,
  Modal
})

let Store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default Store
