import post_data from 'Helper/post_data'

let BASE = '/data/services'

export let get_list = () => {
  return post_data(`${BASE}/list`)
}

export let get_items = (slug) => {
  return post_data(`${BASE}/get_items`, { slug })
}
