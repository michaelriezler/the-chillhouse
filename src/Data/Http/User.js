import post_data from 'Helper/post_data'

export let getInfo = id => {
  return post_data('/data/user/info', { id })
}
