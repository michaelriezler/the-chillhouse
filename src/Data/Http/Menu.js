import post_data from 'Helper/post_data'

export default () => {
  return post_data('/data/menu')
}