import post_data from 'Helper/post_data'

export let getItem = id => {
  return post_data('/data/item/get', { id })
}

export let getItemBySlug = (collection_slug, slug) => {
  return post_data('/data/item/get_by_slug', { collection_slug, slug })
}