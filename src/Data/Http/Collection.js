import post_data from 'Helper/post_data'

let BASE = '/data/collection'

export let get_list = (user, location) => {
  return post_data(`${BASE}/list`, { user, location })
}

export let get_items = id => {
  return post_data(`${BASE}/get_items`, { id })
}

export let get_by_slug = slug => {
  return post_data(`${BASE}/get_by_slug`, { slug })
}
