import post_data from 'Helper/post_data'

export let get_special_offers = () => {
  return post_data('/data/special_offers')
}

export let get_popular = () => {
  return post_data('/data/popular')
}