import { bindActionCreators } from 'redux'
import Store from 'Data/Redux'
import * as A from 'Action'

let d = Store.dispatch

export let UserAction = bindActionCreators({
  init: (user) => ({ type: A.USER_INIT, user })
}, d)

export let ItemAction = bindActionCreators({
  add: (collection, items) => ({ type: A.ITEMS_ADD, collection, items }),
  select: selected => ({ type: A.ITEM_SELECTED, selected })
}, d)

export let CollectionAction = bindActionCreators({
  init: (collections) => ({ type: A.COLLECTION_INIT, collections })
}, d)

export let CartAction = bindActionCreators({
  add: (item, amount) => ({ type: A.CART_ADD, payload: { item, amount } }),
  remove: id => ({ type: A.CART_REMOVE, id }),
  payCash: () => ({ type: A.CART_PAY_CASH })
}, d)

export let MenuAction = bindActionCreators({
  init: (menu) => ({ type: A.MENU_INIT, menu })
}, d)

export let ServiceAction = bindActionCreators({
  init: (services) => ({ type: A.SERVICE_INIT, services }),
  set_items: (slug, items) => ({ type: A.SERVICE_SET_ITEMS, slug, items })
}, d)

export let HomeAction = bindActionCreators({
  set_special: (special_offers) => ({ type: A.HOME_SET_SPECIAL, special_offers }),
  set_popular: (popular) => ({ type: A.HOME_SET_POPULAR, popular }),
}, d)

export let BottomDrawerAction = bindActionCreators({
  open: () => ({ type: A.BOTTOM_DRAWER_OPEN }),
  close: () => ({ type: A.BOTTOM_DRAWER_CLOSE }),
}, d)

export let ModalAction = bindActionCreators({
  open: () => ({ type: A.MODAL_OPEN }),
  close: () => ({ type: A.MODAL_CLOSE }),
}, d)
