import React from 'react'
import ReactDOM from 'react-dom'
import Bootstrap from './Bootstrap'
import * as serviceWorker from './serviceWorker'
import { Provider } from 'react-redux'
import Store from 'Data/Redux'
import { BrowserRouter } from 'react-router-dom'
import AppConfig from 'Context/AppConfig'

import '@material/top-app-bar/dist/mdc.top-app-bar.css'
import '@material/theme/dist/mdc.theme.css'
import '@material/typography/dist/mdc.typography.css'
import '@material/elevation/dist/mdc.elevation.css'
import '@material/list/dist/mdc.list.css'
import '@material/button/dist/mdc.button.css'
import '@material/icon-button/dist/mdc.icon-button.css'
import '@material/linear-progress/dist/mdc.linear-progress.css'
import './index.css'

let app = (
  <BrowserRouter>
    <Provider store={Store}>
      <AppConfig.Provider value={window.APP_CONFIG}>
        <Bootstrap id='dummy_uuid_id' />
      </AppConfig.Provider>
    </Provider>
  </BrowserRouter>
)

ReactDOM.render(app, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register()
