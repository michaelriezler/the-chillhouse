// @flow
import React from 'react'
import styled from 'styled-components'
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import Home from 'Screen/Home'
import Collection from 'Screen/Collection'
import Item from 'Screen/Item'
import Cart from 'Screen/Cart'
import OrderConfirm from 'Screen/OrderConfirm'
import Menu from 'Screen/Menu'
import Services from 'Screen/Services'
import BottomDrawer, { DrawerHeader, DrawerTitle, DrawerContent } from 'Component/BottomDrawer'
import Modal from 'Component/Modal'
import { connect } from 'react-redux'
import { BottomDrawerAction, ModalAction } from 'ActionCreator'
import { Typography } from '@rmwc/typography'
import {
  List,
  ListItem
} from '@rmwc/list';
import useA2HS from 'Hooks/useA2HS'
import A2HS from 'Image/ios_a2hs.png'
import { Button } from '@rmwc/button'

let A2HSImage = styled.img`
  max-width: 100%;
  margin: 0 auto;
  margin-bottom: 24px;
`

let StyledButton = styled(Button)`
  margin: 0 auto;
  display: block!important;
`

let App = (p) => {
  let prompt = useA2HS()

  function addToHS () {
    prompt.prompt()
  }

  return (
    <React.Fragment>
      <Switch>
        <Route path='/order/confirm' component={OrderConfirm} />
        <Route path='/collection/:slug' component={Collection} />
        <Route path='/item/:collection_slug/:slug' component={Item} />
        <Route path='/cart' component={Cart} />
        <Route path='/home' component={Home} />
        <Route path='/menu' component={Menu} />
        <Route path='/services/:slug' component={Services} />
        <Redirect to='/home' />
      </Switch>

      <BottomDrawer open={p.drawer_open} onClose={() => BottomDrawerAction.close()} >
        <DrawerHeader>
          <DrawerTitle>Alexander Springenschmidt</DrawerTitle>
          <Typography use='body2'>alexander.springenschmidt@thechillhouse.com </Typography>
        </DrawerHeader>
        <DrawerContent>
          <List>
            <ListItem>My Activity</ListItem>
            <ListItem>Notification</ListItem>
            <ListItem onClick={addToHS}>Add to Homescreen</ListItem>
            <ListItem>Settings</ListItem>
            <ListItem>Help & Feedback</ListItem>
          </List>
        </DrawerContent>
      </BottomDrawer>

      <Modal open={p.modal_open} onClose={() => ModalAction.close()}>
        <A2HSImage src={A2HS} />
        <StyledButton onClick={() => ModalAction.close()}>Got it</StyledButton>
      </Modal>
    </React.Fragment>
  )
}

export default withRouter(connect(state => {
  return {
    drawer_open: state.BottomDrawer,
    modal_open: state.Modal
  }
})(App))
