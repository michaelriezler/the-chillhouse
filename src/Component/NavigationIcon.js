import styled from 'styled-components'
import { TopAppBarNavigationIcon } from '@rmwc/top-app-bar'

export default styled(TopAppBarNavigationIcon)`
  user-select: none;
`