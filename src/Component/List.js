import styled from 'styled-components'

export default styled.ul`
  padding-left: 24px;
  padding-right: 70px;
`

export let Item = styled.li`
  color: var(--mdc-theme-on-secondary);
  margin-bottom: 24px;
  list-style-type: none;
`