import React from 'react'
import ReactDOM from 'react-dom'
import styled, { createGlobalStyle } from 'styled-components'
import { Typography } from '@rmwc/typography'
import get_container from 'Helper/get_container'

let drawer_container = get_container('js-bottom-container')

let Wrapper = styled.div`
  height: 100vh;
  width: 100vw;
  background: rgba(0,0,0, 0.25);
  position: fixed;
  top: 0;
  left: 0;
  transition: opacity 500ms ease-in;
  ${p => p.open
    ? `
      opacity: 1;
    ` 
    : `
    pointer-events: none
    opacity: 0;
  `}
`

let BottomDrawer = styled.div`
  position: fixed;
  bottom: 0;
  height: 62vh;
  background: #fff;
  border-top-left-radius: 16px;
  border-top-right-radius: 16px;
  transform: translateY(62vh);
  transition: transform 400ms cubic-bezier(0.26, 0.73, 1, 1);
  overflow-y: scroll;
  width: 100%;

  ${p => p.open
    ? `transform: translateY(0)`
    : ''
  }
`

let Global = createGlobalStyle`
  body {
    overflow: ${p => p.open ? 'hidden' : 'normal'};
  }
`

export default ({
    children,
    open = false,
    onClose = () => {}
}) => {

  let Drawer = (
    <React.Fragment>
      <Global open={open} />
      <Wrapper open={open} onClick={onClose} />
      <BottomDrawer open={open}>
        { children }
      </BottomDrawer>
    </React.Fragment>
  )

  return ReactDOM.createPortal(Drawer, drawer_container)
}


export let DrawerHeader = styled.header`
  border-bottom: 1px solid rgba(0,0,0, 0.25);
  padding: 24px;
`

export let DrawerTitle = styled(Typography).attrs({ use: 'body1', tag: 'h3' })`
  margin: 0;
`
export let DrawerContent = styled.div`
  padding: 0 12px;
  border-bottom: 1px solid rgba(0,0,0, 0.25);
`