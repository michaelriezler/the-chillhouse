import React from 'react'
import styled from 'styled-components'
import { Config } from 'Data/Global'
import { Typography } from '@rmwc/typography'
import { IconButton } from '@rmwc/icon-button'
import SearchIcon from 'Component/Icon/Search'
import { BottomDrawerAction } from 'ActionCreator'

let StyledHeader = styled.header`
  position: fixed;
  top: 0;
  width: 100vw;
  heigth: 48px;
  display: flex;
  align-items: space-between;
  background: #fff;
`

let Title = styled(Typography).attrs({ tag: 'h1'})`
  font-size: 1.1em!important;
  width: 100%;
  text-align: center;
  flex-grow: 1;
  margin: 0;
  line-height: 48px!important;
`

let ActionWrapper = styled.div`
  width: 48px;
  flex-shrink: 0;
`

let Avatar = styled.div`
  width: 24px;
  height: 24px;
  margin: 12px;
  border-radius: 12px;
  background: var(--avatar-background, rgb(100, 100, 100));
`

let Search = () => (
  <IconButton
    icon={<SearchIcon />}
  ></IconButton>
)

let Header = ({
  left = <Search />,
  right = <Avatar onClick={() => BottomDrawerAction.open()} />,
  title = Config.name
}) => {

  return (
    <StyledHeader>
      <ActionWrapper>
        { left }
      </ActionWrapper>
      <Title use='headline6'>{title}</Title>
      <ActionWrapper>
        { right }
      </ActionWrapper>
    </StyledHeader>
  )
}

export default Header