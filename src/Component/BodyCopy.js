import React from 'react'
import styled from 'styled-components'
import { Typography } from '@rmwc/typography'

let TextWrapper = styled.div`
  padding-left: 24px;
  padding-right: 56px;
  margin-bottom: 32px;
`

export default ({ children, ...props }) => (
  <TextWrapper {...props}>
    <Typography use='body1'>{children}</Typography>
  </TextWrapper>
)
