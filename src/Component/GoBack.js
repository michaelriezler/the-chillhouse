import React from 'react'
import { IconButton } from '@rmwc/icon-button'
import ArrowBackIcon from 'Component/Icon/ArrowBack'

export default ({ ...props }) => (
  <IconButton
    icon={<ArrowBackIcon />}
    { ...props }
  ></IconButton>
)
