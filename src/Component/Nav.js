import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Icon } from '@rmwc/icon'

let Nav = styled.nav`
  width: 100vw;
  position: fixed;
  bottom: 0;
  left: 0;
  background: #fff;
  height: 48px;
  display: flex;
  box-shadow: 0 0 20px -3px rgba(0,0,0, 0.2);
  justify-content: space-around;
`

let NavItem = styled(Link)`
  display: flex;
  flex-direction: column;
  align-items: center;
  color: var(--mdc-theme-primary);
  text-decoration: none;
  font-family: 'Noto', sans-serif;
  font-size: 0.7em;
  justify-content: center;
`

export default ({
  title = '',
  slot = {}
}) => (
  <Nav>
    <NavItem to='/'>
      <Icon icon='home'></Icon>
      Home
    </NavItem>
    <NavItem to='/menu'>
      <Icon icon='view_list'></Icon>
      Menu
    </NavItem>
    <NavItem to='/gallery'>
      <Icon icon='view_module'></Icon>
      Gallery
    </NavItem>
  </Nav>
)
