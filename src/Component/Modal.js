import React from 'react'
import ReactDOM from 'react-dom'
import styled, { createGlobalStyle } from 'styled-components'
import get_container from 'Helper/get_container'

let container = get_container('js-modal')

let Wrapper = styled.div`
  height: 100vh;
  width: 100vw;
  background: rgba(0,0,0, 0.25);
  position: fixed;
  top: 0;
  left: 0;
  transition: opacity 500ms ease-in;
  ${p => p.open
    ? `
      opacity: 1;
    ` 
    : `
    pointer-events: none
    opacity: 0;
  `}
`

let ContentWrapper = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;
  padding: 24px;
  align-items: center;
  position: fixed;
  top: 0;
  left: 0;
  opacity: ${p => p.open ? '1' : '0'};
  pointer-events: none;
`

let Content = styled.div`
  width: 100%;
  background: #fff;
  padding: 24px;
  border-radius: 16px;
`

let Global = createGlobalStyle`
  body {
    overflow: ${p => p.open ? 'hidden' : 'normal'};
  }
`

export default ({
  open = true,
  children,
  onClose = () => {}
}) => {

  let Modal = (
    <React.Fragment>
      <Global open={open} />
      <Wrapper open={open} onClick={onClose}/>
      <ContentWrapper open={open}>
        <Content>
          { children }
        </Content>
      </ContentWrapper>
    </React.Fragment>
  )

  return ReactDOM.createPortal(
    Modal,
    container
  )
}