import React from 'react'
import styled from 'styled-components'
import { withRouter, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { Typography } from '@rmwc/typography'
import { IconButton } from '@rmwc/icon-button'
import ShoppingCartIcon from 'Component/Icon/ShoppingCart'

let StyledLink = styled(Link)`
  text-decoration: none;
  color: var(--mdc-theme-primary);
`

let Badge = styled.span`
  --size: 20px;
  position: absolute;
  top: 3px;
  right: 3px;
  width: var(--size);
  height: var(--size);
  z-index: 1;
  background: var(--highlight);
  border-radius: 100%;
  text-align: center;
  line-height: var(--size);
  font-size: 10px;
  user-select: none;
  color: #fff;

  span {
    user-select: none;
  }
`

let Wrapper = styled.div`
  position: relative;
`

let ShoppingCart = ({ items, ...props }) => {
  return (
    <Wrapper {...props}>
      
      <StyledLink to='/cart'>
        <IconButton
          icon={<ShoppingCartIcon/>}
        ></IconButton>
      </StyledLink>
      
      { items > 0 &&
        <Badge>
          <Typography>{items}</Typography>
        </Badge>
      }
    </Wrapper>
  )
}

export default withRouter(connect(state => {
  return {
    items: state.Cart.items.length
  }
})(ShoppingCart))