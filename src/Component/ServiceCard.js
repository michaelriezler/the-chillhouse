import React from 'react'
import styled from 'styled-components'
import { Typography } from '@rmwc/typography'
import { Link } from 'react-router-dom'

let Img = styled.div`
  width: 100%;
  height: 0;
  padding-bottom: 60%;
  background: rgb(100, 100, 100);
  border-radius: 8px;
  margin-bottom: 16px;
`

let StyledServiceCard = styled(Link)`
  --mdc-theme-primary: var(--highlight);
  text-decoration: none;
  color: inherit;
`

let ServiceCardInfo = styled.div`
  display: flex;
`

let ServiceCardAction = styled.div`
  flex-shrink: 0;
`

let ServiceCardTitle = styled(Typography).attrs({ use: 'body1' })`
  flex-grow: 1;
  margin-right: 16px;
`

let Action = ({ children }) => {

  let onClick = node => event => {
    event.preventDefault()
    node.props.onClick && node.props.onClick(event)
  }

  let c = React.Children.toArray(children)
    .filter(node => React.isValidElement(node))
    .map(node => React.cloneElement(node, {
      onClick: onClick(node)
    }))

  if (c.length === 0) {
    return null
  }

  return (
    <ServiceCardAction>
      { c }
    </ServiceCardAction>
  )
}

let ServiceCard = ({ title, action = null, slug }) => {

  return (
    <StyledServiceCard to={`/service/${slug}`}>
      <Img />
      <ServiceCardInfo>
        <ServiceCardTitle>{ title }</ServiceCardTitle>
        <Action>{action}</Action>
      </ServiceCardInfo>
    </StyledServiceCard>
  )
}

export default ServiceCard