import React from 'react'
import styled from 'styled-components'
import { Elevation } from '@rmwc/elevation'
import { Typography } from '@rmwc/typography'

let Card = styled(Elevation).attrs({ z: 7 })`
  --width: 70vw;
  width: var(--width);
  margin-right: 32px;
  height: calc(var(--width) * 1.2);
  background: var(--app-card, #fafafa);
  flex-shrink: 0;
  border-radius: 8px;
  padding: 16px;
  display: flex;
  flex-direction: column;
  color: var(--mdc-theme-on-secondary);
`

export default ({
  title = '',
  subtitle = '',
  ...props
}) => (
  <Card {...props}>
    <Typography use='subtitle2' style={{ marginTop: 'auto' }}>
      { subtitle }
    </Typography>
    <Typography use='headline5' tag='h3' style={{ margin: 0 }}>
      { title }
    </Typography>
  </Card>
)
