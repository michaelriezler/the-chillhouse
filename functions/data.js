let f = require('faker')

let uuid = () => f.random.uuid()
let to_slug = str => str.toLowerCase().split(' ').join('-')

let Slug = {
  Sides: 'sides',
  HotStuff: 'hot-stuff',
  Sashimi: 'sashimi',
  Nigiri: 'nigiri',
  SushiRolls: 'sushi-rolls',
  PokeBowls: 'poke-bowls',
  SweetStuff: 'sweet-stuff',
  Drinks: 'drinks'
}

let assignMeta = item => Object.assign(item, {
  id: uuid(),
  slug: to_slug(item.name),
  short_description: f.lorem.sentence()
})

let Item = (name, price) => assignMeta({ name, price })

let Items = {
  [Slug.Sides]: [
    Item('Potato Salad', 20),
    Item('Gomae Salad', 20),
    Item('Seeweed Salad', 25),
    Item('Miso Soup', 15),
    Item('Edamae', 15),
    Item('Poke Nachos', 45),
    Item('Tempura Platter', 48),
    Item('Karaage Fried Chicken', 45),
    Item('Agedashi Tofu', 35),
  ],
  [Slug.HotStuff]: [
    Item('Katsudon', 65),
    Item('Katsu Curry', 70),
    Item('Udon', 65),
    Item('Fish Taco Plate', 65),
  ],
  [Slug.Sashimi]: [
    Item('Tuna', 42),
    Item('Snapper', 42),
    Item('Salmon', 58),
  ],
  [Slug.Nigiri]: [
    Item('Tuna', 26),
    Item('Snapper', 35),
    Item('Salmon', 29),
  ],
  [Slug.SushiRolls]: [
    Item('Tuna', 34),
    Item('Snapper', 35),
    Item('Salmon', 40),
    Item('Spicy Shrimp', 38),
    Item('Spicy Tofu', 28),
    Item('Cucumber', 28),
    Item('Jackfruit Katsu', 28),
    Item('Rainbow Komodo Roll', 90),
    Item('Red Komodo Roll', 85),
    Item('Green Komodo Roll', 80),
  ],
  [Slug.PokeBowls]: [
    Item('Tuna', 78),
    Item('Snapper', 78),
    Item('Salmon', 82),
    Item('Spicy Shrimp', 82),
    Item('Spicy Tofu', 70),
    Item('Avocado And Cucumber', 70),
    Item('Jackfruit Katsu', 70),
    Item('Extra Bowl of Rice', 10),
  ],
  [Slug.SweetStuff]: [
    Item('Vegan Motchi Ice Cream', 25),
    Item('Brownie S More', 50),
  ],
  [Slug.Drinks]: [
    Item('Mineral Water', 20),
    Item('Green Tea', 20),
    Item('Soda', 20),
    Item('Bintang Draft', 35),
    Item('Sapporo', 65),
    Item('Beervana Craft Beer', 100),
    Item('White Wine', 80),
    Item('Red Wine', 80),
    Item('Soju', 160),
    Item('Sake', 160),
  ],
}

let Collections = [{
  id: uuid(),
  name: 'Sides',
  items: Items[Slug.Sides].length,
  slug: Slug.Sides
}, {
  id: uuid(),
  name: 'Hot Stuff',
  items: Items[Slug.HotStuff].length,
  slug: Slug.HotStuff
}, {
  id: uuid(),
  name: 'Sashimi',
  items: Items[Slug.Sashimi].length,
  slug: Slug.Sashimi
}, {
  id: uuid(),
  name: 'Nigiri',
  items: Items[Slug.Nigiri].length,
  slug: Slug.Nigiri,

}, {
  id: uuid(),
  name: 'Sushi Rolls',
  items: Items[Slug.SushiRolls].length,
  slug: Slug.SushiRolls
}, {
  id: uuid(),
  name: 'Poke Bowls',
  items: Items[Slug.PokeBowls].length,
  slug: Slug.PokeBowls
}, {
  id: uuid(),
  name: 'Sweet Stuff',
  items: Items[Slug.SweetStuff].length,
  slug: Slug.SweetStuff
}, {
  id: uuid(),
  name: 'Drinks',
  items: Items[Slug.Drinks].length,
  slug: Slug.Drinks
}]

let ServiceType = {
  BusinessFacilities: 'business-facilities',
  Transport: 'transport',
  ReceptionServices: 'reception-services',
  Outdoors: 'outdoors',
  EntertainmentFamily: 'entertainment-family',
  PoolWellness: 'pool-wellness',
  General: 'general',
  Activities: 'activities',
  CleaningServices: 'cleaning-services'
}

let Service = (name, charge) => assignMeta({ name, charge })

let Services = {
  [ServiceType.CleaningServices]: [
    Service('Daily maid service'),
    Service('Dry cleaning', true),
    Service('Laundry', true),
  ],

  [ServiceType.BusinessFacilities]: [
    Service('Fax/photocopying', true),
  ],

  [ServiceType.Transport]: [
    Service('Airport drop off', true),
    Service('Airport pick up', true),
  ],

  [ServiceType.ReceptionServices]: [
    Service('Concierge service'),
    Service('ATM/cash machine on site'),
    Service('Luggage storage'),
    Service('Tour desk'),
    Service('Currency exchange'),
    Service('24-hour front desk'),
  ],

  [ServiceType.Outdoors]: [
    Service('Beachfront'),
    Service('Sun terrace'),
    Service('Private beach area'),
    Service('BBQ facilities', true),
    Service('Terrace'),
    Service('Garden'),
  ],

  [ServiceType.EntertainmentFamily]: [
    Service('Babysitting and child services', true)
  ],

  [ServiceType.PoolWellness]: [
    Service('Massage', true),
    Service('Spa and wellness centre', true),
    Service('Fitness centre'),
    Service('Swimming pool'),
    Service('Fitness classes'),
    Service('Yoga classes'),
    Service('Fitness'),
    Service('Spa Facilities'),
    Service('Sun loungers or beach chairs'),
    Service('Shallow end'),
    Service('Pool bar'),
    Service('Infinity pool'),
    Service('Swimming pool toys'),
    Service('Outdoor pool'),
  ],

  [ServiceType.General]: [
    Service('Shuttle service'),
    Service('Shared lounge/TV area'),
    Service('Airport shuttle', true),
    Service('Shuttle service', true),
    Service('Air conditioning'),
    Service('Shops (on site)'),
    Service('Car hire'),
    Service('Gift shop'),
    Service('Safety deposit box'),
    Service('Family rooms'),
    Service('Barber/beauty shop'),
    Service('Non-smoking rooms'),
    Service('Newspapers'),
    Service('Room service '),
  ],

  [ServiceType.Activities]: [
    Service('Live sport events (broadcast)'),
    Service('Cooking class Off-site', true),
    Service('Tour or class about local culture', true),
    Service('Happy hour', true),
    Service('Themed dinner nights', true),
    Service('Movie nights'),
    Service('Beach'),
    Service('Evening entertainment'),
    Service('Kids club', true),
    Service('Snorkelling', true),
    Service('Horse riding', true),
    Service('Diving', true),
    Service('Table tennis'),
    Service('Fishing', true),
  ]
}

let SC = (title, slug, items) => {
  return {
    id: uuid(),
    title,
    slug,
    items: Services[slug].length
  }
}

let ST = ServiceType
let ServiceCollection = [
  SC('General', ST.General),
  SC('Activities', ST.Activities),
  SC('Cleaning', ST.CleaningServices),
  SC('Transport', ST.Transport),
  SC('Outdoors', ST.Outdoors),
  SC('Reception', ST.ReceptionServices),
  SC('Entertainment', ST.EntertainmentFamily),
  SC('Wellness', ST.PoolWellness),
  SC('Business', ST.BusinessFacilities),
]


let Offer = (type, index) => {
  let { name } = Services[type][index]
  return assignMeta({
    name
  })
}

let SpecialOffers = [
  Offer(ST.PoolWellness, 0),
  Offer(ST.CleaningServices, 1),
  Offer(ST.Transport, 0),
  Offer(ST.Activities, 1),
  Offer(ST.Activities, 11),
]

let Popular = [
  Offer(ST.Activities, 10),
  Offer(ST.Activities, 9),
  Offer(ST.PoolWellness, 1),
]

module.exports = {
  Slug,
  Collections,
  Items,
  ServiceType,
  Services,
  ServiceCollection,
  SpecialOffers,
  Popular
}