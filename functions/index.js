let functions = require('firebase-functions')
let app = require('./app')

exports.app = functions.https.onRequest(app)
