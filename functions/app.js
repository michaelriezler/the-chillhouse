let express = require('express')
let bodyParser = require('body-parser')
let cors = require('cors')
let f = require('faker')
let {
  Collections,
  Items,
  ServiceCollection,
  Services,
  SpecialOffers,
  Popular
} = require('./data')

let app = express()
app.use(cors({ origin: true }))
app.use(bodyParser.json())

let collection = express.Router()

collection.post('/list', (req, res) => {
  res.json(Collections)
})

collection.post('/get_items', (req, res) => {
  let { id } = req.body
  let collection = Collections.find(c => c.id === id)
  let payload = Items[collection.slug]

  res.json(payload)
})

collection.post('/get_by_slug', (req, res) => {
  let { slug } = req.body

  let c = Collections.find(c => c.slug === slug)

  res.json(c)
})

app.use('/data/collection', collection)

let user = express.Router()

user.post('/info', (req, res) => {
  let { id } = req.body

  let payload = {
    id: f.random.uuid(),
    name: `TYGR Sushi`,
    logo: 'http://tygrsushi.com/wp-content/themes/tygr/dist/images/tygr-full.png',
    description: `
      Welcome to TYGR Sushi Hand Roll Bar. We’re serving just-made hand rolls with warm nasi, crispy nori, and fresh fillings. Our rolls are designed to be eaten immediately after being served by our hand roll specialists and enjoyed with our signature sauces.
    `
  }

  res.json(payload)
})

app.use('/data/user', user)

let make_item_payload = (item) => {
  return Object.assign({}, item, {
    description: f.lorem.sentences(),
    ingredients: Array(Math.floor(Math.random() * 8) + 1)
      .fill(0)
      .map(_ => f.lorem.word())
  })
}

let item = express.Router()

item.post('/get', (req, res) => {
  let { id } = req.body

  let item = Object.values(Items)
    .filter(c => c.some(item => item.id === id))
    .reduce((acc, x) => acc.concat(x), [])
    .find(item => item.id === id)

  let payload = make_item_payload(item)
  res.json(payload)
})

item.post('/get_by_slug', (req, res) => {
  let { collection_slug, slug } = req.body
  let items = Items[collection_slug]
  let item = items.find(i => i.slug === slug)
  let payload = make_item_payload(item)
  res.json(payload)
})

app.use('/data/item', item)

let services = express.Router()

services.post('/list', (req, res) => {
  res.json(ServiceCollection)
})

services.post('/get_items', (req, res) => {
  let { slug } = req.body
  let payload = Services[slug]
  res.json(payload)
})

services.post('/get_by_slug', (req, res) => {
  let { slug } = req.body

  let c = ServiceCollection.find(c => c.slug === slug)

  res.json(c)
})

let make_service_payload = (item) => {
  return Object.assign({}, item, {
    description: f.lorem.sentences()
  })
}

services.post('/get_item', (req, res) => {
  let { service_slug, slug } = req.body
  let items = Services[service_slug]
  let item = items.find(i => i.slug === slug)
  let payload = make_service_payload(item)
  res.json(payload)
})

app.use('/data/services', services)

let menu = express.Router()

menu.post('/', (req, res) => {
  let result = Collections.map(c => {
    return Object.assign({}, c, {
      total: c.items,
      items: Items[c.slug]
    })
  })

  res.json(result)
})

app.use('/data/menu', menu)

app.post('/data/special_offers', (req, res) => {
  res.json(SpecialOffers)
})

app.post('/data/popular', (req, res) => {
  res.json(Popular)
})

module.exports = app